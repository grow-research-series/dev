jQuery(document).ready(function ($) {

    
    var growNavigation = {

        init : function () {
            this.cacheDom();
            this.bindEvents();
        },
        
        cacheDom : function () {
            this.document = $(document);
            this.page = this.document.find('.grow-site-container');
            this.growNavigationContainer = this.document.find('.grow-navigation-container');
            this.growNavigationMenuButtonOpen = this.document.find('.grow-navigation-menu-button-open');
            this.growNavigationMenuButtonClose = this.document.find('.grow-navigation-menu-button-close');
            
            this.growNavigationContainerFirstFocusable = this.growNavigationContainer.find( 'select, input, textarea, button, a' ).filter( ':visible' ).first();
            this.growNavigationContainerLastFocusable = this.growNavigationContainer.find( 'select, input, textarea, button, a' ).filter( ':visible' ).last();
                        
        },
        
        
        
        bindEvents : function () {
            this.growNavigationMenuButtonOpen.on('click', this.show.bind(this));
            this.growNavigationMenuButtonClose.on('click', this.hide.bind(this));
            this.document.keyup(this.keyUpHandler.bind(this));
            
            this.growNavigationContainerFirstFocusable.on('keydown', this.tabKeyMenuFirstChildHandler.bind(this));
            this.growNavigationContainerLastFocusable.on('keydown', this.tabKeyMenuLastChildHandler.bind(this));
        },
        
        
        
        tabKeyMenuFirstChildHandler : function (e) { 
        
            if ( ( e.keyCode === 9 && e.shiftKey ) && (this.growNavigationContainer.hasClass('open') === true) ) {

                e.preventDefault();

                this.growNavigationContainerLastFocusable.focus();
            }
        
        }, 
        
        tabKeyMenuLastChildHandler : function (e) { 
        
            if ( ( e.keyCode === 9 && !e.shiftKey ) && (this.growNavigationContainer.hasClass('open') === true) ) {

                e.preventDefault();

                this.growNavigationContainerFirstFocusable.focus(); 

            }
        }, 
        
        show : function () {
            this.growNavigationContainer.addClass('open');
            
            // Set focus after a delay to allow for the annimation
            window.setTimeout(this.setFocusAfterOpen.bind(this), 300);
            
            console.log("Open Menu");
            
        },
        
        
        setFocusAfterOpen : function () { 
            this.growNavigationMenuButtonClose.focus();
        },
        
        hide : function () {
            this.growNavigationContainer.removeClass('open');
            this.growNavigationMenuButtonOpen.focus();
            
            console.log("Close Menu");
        
        },
        
        keyUpHandler : function (e) {
            
            // If escape is pressed and the menu is open then close it
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                if (this.growNavigationContainer.hasClass('open') === true) {
                    this.hide();
                }
            }
                
        }
        
    };
    
    
    // Initialise the Grow Navigation Object
    growNavigation.init();
    
});