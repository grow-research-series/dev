---
title: Working Papers
linktitle: Working Papers
description: Grow Research Project - Working Papers

date: 2017-02-01
publishdate: 2017-02-01
lastmod: 2017-02-01

categories: []
#tags: []

draft: false

weight: 1
---

<div>
    <section>
        <h2>Information</h2>
        <div class="lead">
        <p>GrOW working papers are pre-publication versions of scholarly papers, aimed at stimulating public discussion of research and advancing scientific knowledge on women’s economic empowerment and economic growth. We have four separate categories of papers:</p>
        <ul>
            <li>Research contribution papers are addressed to experts and intended to generate maximum policy uptake.</li>
            <li>Research reports are written as more broadly accessible, and may be more technical or descriptive in nature.</li>
            <li>Linked papers are existing papers that may already be published elsewhere and that authors would like featured on our website.</li>
            <li>Concept papers are in-depth reviews or discussion papers on core topics pertaining to women’s economic empowerment and economic growth. Concept papers are procured by invitation only. We do not accept concept papers through our regular submission process.</li>
        </ul>
        </div>
    </section>
    <section>
        <h2>Working Papers</h2>
        {{< working-papers >}}
    </section>
</div>