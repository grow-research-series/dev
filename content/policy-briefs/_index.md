---
title: Policy Briefs
linktitle: Policy Briefs
description: Grow Research Project - Policy Briefs

date: 2017-02-01
publishdate: 2017-02-01
lastmod: 2017-02-01

categories: []
#tags: []

draft: false
weight: 2   
---

<div>
<section>
    <h2>Information About GrOW Research Policy Briefs</h2>
    <p class="lead">GrOW Research policy briefs are concise documents that synthesize research results, with special attention paid to policy implications for women’s economic growth and economic empowerment. Policy briefs are companion pieces to existing working papers.</p>
</section>
<section>
    <h2>Our Policy Briefs</h2>
    {{< policy-briefs >}}
</section>
<section>
    <h2>IDRC Policy Briefs</h2>
    <p>On this page you can also find policy briefs showcasing the results of IDRC’s Growth and Economic Opportunities for Women program, taking place in 14 countries around the world.</p>
    <div class="row">
        {{< policy-brief 
                img-src = "/images/policy-briefs/idrc-5.jpg"
                pub-href = "https://idl-bnc-idrc.dspacedirect.org/bitstream/handle/10625/56368/IDL56368.pdf?sequence=2&amp;isAllowed=y"
                title = "From school to work in six African countries: how are women faring?"
                author = "Andy McKay, Mary O'Neill, Alejandra Vargas and Martha Melesse">}}
        {{< policy-brief 
                img-src = "/images/policy-briefs/idrc-4.jpg"
                pub-href = "https://idl-bnc-idrc.dspacedirect.org/bitstream/handle/10625/56366/IDL56366.pdf?sequence=2&amp;isAllowed=y"
                title = "How to grow women-owned businesses"
                author = "Mary O'Neill, Alejandra Vargas and Arjan de Haan">}}
        {{< policy-brief 
                img-src = "/images/policy-briefs/idrc-3.jpg"
                pub-href = "https://idl-bnc-idrc.dspacedirect.org/bitstream/handle/10625/56360/IDL-56360.pdf?sequence=2&amp;isAllowed=y"
                title = "Increasing women’s support for democracy in Africa"
                author = "Stephan Klasen, Mary O’Neill and Alejandra Vargas">}}
        {{< policy-brief 
                img-src = "/images/policy-briefs/idrc-2.jpg"
                pub-href = "https://idl-bnc-idrc.dspacedirect.org/bitstream/handle/10625/56362/IDL56362.pdf?sequence=2&amp;isAllowed=y"
                title = "Reducing child marriage and increasing girls’ schooling in Bangladesh"
                author = "Nina Buchmann, Rachel Glennerster, Mary O'Neill and Alejandra Vargass">}}
        {{< policy-brief 
                img-src = "/images/policy-briefs/idrc-1.jpg"
                pub-href = "https://idl-bnc-idrc.dspacedirect.org/bitstream/handle/10625/56369/IDL56369.pdf?sequence=2&amp;isAllowed=y"
                title = "Unpaid care and women’s empowerment: lessons from research and practice"
                author = "Mary O’Neill, Alejandra Vargas and Deepta Chopra">}}
    </div>
</section>
</div>