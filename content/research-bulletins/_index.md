---
title: Research Bulletins
linktitle: Research Bulletins
description: Grow Research Project - Research Bulletins

date: 2017-02-01
publishdate: 2017-02-01
lastmod: 2017-02-01

categories: []
#tags: []

draft: false
weight: 3
---

<div>
<section>
    <h2>Our Bulletins</h2>
    <div class="lead">
        <p>The GrOW Research Bulletin is a bi-monthly publication featuring current news, research, interviews and  discussion pieces on women’s economic empowerment written by Canadian development scholars and practitioners.</p>
        <p>Individuals interested in submitting a discussion piece for an upcoming issue can contact our Managing Editor, Kate Grantham, at <a href="mailto:kathleen.grantham@mcgill.ca">kathleen.grantham@mcgill.ca</a>.</p>
    </div>
    {{< research-bulletins >}}
</section>
</div>