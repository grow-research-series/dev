---
title: Submit
linktitle: Submit
description: Grow Research Project - Submit Papers

date: 2017-02-01
publishdate: 2017-02-01
lastmod: 2017-02-01

categories: []
#tags: []

draft: false
weight: 4
---

<div>
<section>
    <h2>Submit your work</h2>
    <div class="lead">
        <p>We invite working paper submissions on a variety of topics related to women's economic empowerment and economic growth.</p>
        <p>Submitting your paper to the series does not preclude you from eventually submitting your work for consideration in a scholarly journal, as you will retain full copyrights to your manuscript. There are many benefits of submitting a paper. Authors will receive:</p>
        <ul>
            <li>The opportunity to showcase your research in an online, open access platform and get visibility before the paper appears in a scholarly journal</li>
            <li>Copy-editing services, including: proofreading, formatting and bibliographic checking</li>
            <li>Optional double blind peer-review process to increase your future chances of success publishing in a scholarly journal</li>
            <li>Editorial assistance to generate a 2-4-page policy brief for your working paper</li>
        </ul>
        <p>Although we do not guarantee dissemination within a specific timeframe, the average length of time from submission to dissemination online to date is one month for working papers that receive copy-editing only, and four months for working papers that undergo peer review.</p>
        <p>Before submitting your paper, please read the following documents:</p>
        <ul>
            <li><a href='{{< relreffix "/publications/submit-papers/authorguidelines.pdf" >}}'>Author Guidelines (PDF)<span class="badge badge-secondary"> New</span></a></li>
            <li><a href='{{< relreffix "/publications/submit-papers/styleguide.pdf" >}}'>Style Guide (PDF)<span class="badge badge-secondary"> New</span></a></li>
        </ul>
        <p>Paper submissions and any questions about our submission process can be emailed to our Managing Editor, Kate Grantham at: <a href="mailto:kathleen.grantham@mcgill.ca">kathleen.grantham@mcgill.ca</a></p>
    </div>
</section>
</div>