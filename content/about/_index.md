---
title: About
linktitle: About
description: Information about the Grow Research Project

date: 2017-02-01
publishdate: 2017-02-01
lastmod: 2017-02-01

categories: []
#tags: []

draft: false

weight: 7

---

<div>
<section>
    <h2>Information</h2>
    <div class="lead">
    <p>The GrOW Research Series brings together scholarly research on women’s economic empowerment and economic growth in low-income countries. Through the dissemination of working papers, policy briefs and other original scholarship, our website serves as an online, open access platform to showcase current research with a view to promoting evidence-based policy-making. </p>
    <p>We are also the official, though not exclusive, research platform for the <a href="https://www.idrc.ca/en/initiative/growth-and-economic-opportunities-women" rel="external">Growth and Economic Opportunities for Women (GrOW) program</a>, a multi-funder partnership between the UK Government's Department for International Development, The William and Flora Hewlett Foundation, and Canada’s International Development Research Centre. With 14 projects in 50 countries, the GrOW program works with researchers to improve economic outcomes and opportunities for poor women on the themes of employment, the care economy, and women’s economic agency. Each of these projects is generating new data on different dimensions of women’s economic empowerment, and the GrOW Research Series serves as a host, or repository, for this global body of evidence.</p>
    <p>This series is housed at the Institute for the Study of International Development (ISID) at McGill University in Montreal, Canada.</p>
    </div>
    <div class="row justify-content-around align-items-center no-gutters">
        <div class="col-7 col-sm-3 mt-3 mt-sm-0">
            <a rel="external" href="http://www.mcgill.ca/isid/">
                <img class="img-fluid" src='{{< relreffix "/images/about/isid.png" >}}' alt="" />  
                <span class="sr-only">Institute for the Study of International Development</span>
            </a>
        </div>
        <div class="col-7 col-sm-3 my-3 my-sm-1">
            <a href='{{< relreffix "/" >}}'>
                <img class="img-fluid" src='{{< relreffix "/images/about/grow-brand.png" >}}' alt="" />
                <span class="sr-only">GroW Research Logo</span>
            </a>
        </div>
        <div class="col-7 col-sm-3 mb-3 mb-sm-0">
            <a rel="external" href="http://www.mcgill.ca/">
                <img class="img-fluid" src='{{< relreffix "/images/about/mcgill.png" >}}' alt="" />
                <span class="sr-only">McGill University</span>
            </a>
        </div>
    </div>
</section>
<section>
    <h2>Contact Us</h2>
    <div class="row align-items-center">
        <div class="col-7 col-sm-6">
            <p><strong>GrOW Research Series</strong><br />
            Institute for the Study of International Development<br />
            Peterson Hall, 3460 McTavish St.<br />
            Montreal, Quebec<br />
            H3A 0E6</p>
            <p><a href="mailto:grow.research@mcgill.ca">grow.research@mcgill.ca</a></p>
        </div>
        <div class="col-5 col-sm-6">
            <svg xmlns="http://www.w3.org/2000/svg" class="g-svg-icon-style-1 svg-icon" viewBox="0 0 20 20">
                <path fill="none" d="M 17.051 3.302 H 2.949 c -0.866 0 -1.567 0.702 -1.567 1.567 v 10.184 c 0 0.865 0.701 1.568 1.567 1.568 h 14.102 c 0.865 0 1.566 -0.703 1.566 -1.568 V 4.869 C 18.617 4.003 17.916 3.302 17.051 3.302 Z M 17.834 15.053 c 0 0.434 -0.35 0.783 -0.783 0.783 H 2.949 c -0.433 0 -0.784 -0.35 -0.784 -0.783 V 4.869 c 0 -0.433 0.351 -0.784 0.784 -0.784 h 14.102 c 0.434 0 0.783 0.351 0.783 0.784 V 15.053 Z M 15.877 5.362 L 10 9.179 L 4.123 5.362 C 3.941 5.245 3.699 5.296 3.581 5.477 C 3.463 5.659 3.515 5.901 3.696 6.019 L 9.61 9.86 C 9.732 9.939 9.879 9.935 10 9.874 c 0.121 0.062 0.268 0.065 0.39 -0.014 l 5.915 -3.841 c 0.18 -0.118 0.232 -0.36 0.115 -0.542 C 16.301 5.296 16.059 5.245 15.877 5.362 Z" ></path>
            </svg>
        </div>
    </div>
</section>
<section>
    <h2>Our Editorial Board</h2>
    <div class="row justify-content-center">
        {{< editorial-board-member 
                img-src = "/images/members/1.jpg"
                name = "Dr. Kate Grantham"
                title = "Managing Editor, GrOW Research Series<br />Research Associate, Institute for the Study of International Development<br />McGill University"
                twitter = "KateGrantham"
                email = "kathleen.grantham@mcgill.ca"
        >}}
        {{< editorial-board-member 
                img-src = "/images/members/2.jpg"
                name = "Dr. Sonia Laszlo"
                title = "Director, Institute for the Study of International Development<br />Associate Professor, Economics<br />McGill University"
                twitter = "sclaszlo"
                website = "https://sites.google.com/site/sonialaszlo/home"
        >}}
        {{< editorial-board-member 
                img-src = "/images/members/4.jpg"
                name = "Dr. Franque Grimard"
                title = "Associate Professor, Economics<br />McGill University "
        >}}
        {{< editorial-board-member 
                img-src = "/images/members/3.jpg"
                name = "Dr. Kathleen Fallon"
                title = "Graduate Program Director and Professor, Sociology<br />Stony Brook University "
        >}}
    </div>
</section>
</div>